'''
    pyCoDa init script
'''

__title__ = "pyCoDa"
__author__ = "Christian Brinch"
__email__ = "cbri@food.dtu.dk"
__copyright__ = "Copyright 2019 C. Brinch"
__version__ = 0.5
__all__ = ['pycoda', 'extra', 'plot']

from . import plot
from .pycoda import *
init()
