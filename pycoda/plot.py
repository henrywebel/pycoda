# -*- coding: utf-8 -*-
''' Compositional plots
'''

__author__ = "Christian Brinch"
__copyright__ = "Copyright 2019"
__credits__ = ["Christian Brinch"]
__license__ = "AFL 3.0"
__version__ = "0.5"
__maintainer__ = "Christian Brinch"
__email__ = "cbri@food.dtu.dk"

from importlib import reload
import ternary as td
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from pycoda import extra

reload(extra)


def _scree_plot(axis, eig_val):
    axis.set_xlabel('Component')
    axis.set_ylabel('Explained varaince')
    axis.set_xlim(0, min(len(eig_val)+1, 20))
    axis.bar(np.arange(len(eig_val))+1, (eig_val/np.sum(eig_val))**2)
    csum = np.cumsum(eig_val**2/np.sum(eig_val**2))
    for i in range(min(5, len(eig_val))):
        axis.annotate(str(np.round(csum[i]*100))+'%',
                      (i+1.2, (eig_val[i]/np.sum(eig_val))**2))


def _plotloadings(axis, loadings, plotloadings, scale):
    loadingnames = plotloadings
    if plotloadings in ['labels', 'both']:
        loadingnames = loadings.columns
    for idx, column in enumerate(loadings):
        axis.arrow(0, 0,
                   loadings.loc['pc1'][idx],
                   loadings.loc['pc2'][idx],
                   facecolor='grey',
                   alpha=0.7,
                   lw=0,
                   width=scale*0.01)
        if column in loadingnames and np.sqrt(pow(loadings.loc['pc1'][idx], 2)
                                              + pow(loadings.loc['pc2'][idx], 2)) > 1.:
            axis.annotate(column, (loadings.loc['pc1'][idx]+0.005,
                                   loadings.loc['pc2'][idx]),
                          color='black',
                          ha='left',
                          va='bottom',
                          alpha=0.8,
                          fontsize=12)


def _plotscores(axis, scores, descr, legend, labels, plotellipses, colors):
    if not colors and descr is not None:
        temp = plt.cm.tab20(np.linspace(0, 1, len(set(descr))))
        colors = {}
        for idx, group in enumerate(set(descr)):
            colors[group] = temp[idx]

    if descr is not None:
        for group in set(descr):
            # if group in ['Europe & Central Asia', 'Sub-Saharan Africa']:
            idx = descr.loc[descr == group].index
            axis.plot(*scores[idx].values, '.', alpha=0.7,
                      label=group, color=colors[group])

            if plotellipses and len(idx) > 3:
                ellipse = extra.get_covariance_ellipse(pd.DataFrame(scores[idx].values.T),
                                                       conf=95)
                extra.plot_covariance_ellipse(axis, ellipse, color=colors[group])

                if labels == 'sparse':
                    _ = [axis.text(scores.loc['pc1'][point]+0.005,
                                   scores.loc['pc2'][point], point,
                                   fontsize=10, alpha=0.8)
                         for point in idx
                         if extra.check_point_in_ellipse(scores[point], ellipse)]

            if labels == 'all':
                _ = [axis.text(scores.loc['pc1'][i]+0.005,
                               scores.loc['pc2'][i], i,
                               fontsize=10, alpha=0.8) for i in idx]
            if legend:
                axis.legend(frameon=False, markerscale=2.5)
    else:
        axis.plot(*scores.values, 'o', alpha=0.7, color='steelblue')


def _svd(clr):
    scores, eig_val, loadings = np.linalg.svd(clr)
    scores = pd.DataFrame(scores.T[0:2, :], columns=clr.index, index=['pc1', 'pc2'])
    loadings = pd.DataFrame(np.inner(eig_val*np.identity(len(eig_val)),
                                     loadings.T[0:len(eig_val), 0:len(eig_val)])[0:2],
                            columns=clr.columns[0:len(eig_val)], index=['pc1', 'pc2'])
    return scores, eig_val, loadings


def pca(data, descr=None, scree=True, labels=None, colors=None, plotloadings=None,
        plotellipses=True, legend=True):
    ''' Plot a PCA biplot '''

    scores, eig_val, loadings = _svd(data.coda.center().coda.clr())

    fig = plt.figure(1)
    plt.clf()
    if scree:
        frames = [[0.08, 0.08, .22, .90], [0.35, 0.08, .63, .90]]
        axis = (fig.add_axes(frames[0], frame_on=False), fig.add_axes(frames[1], frame_on=True))
        _scree_plot(axis[0], eig_val)
        p = 1
    else:
        frames = [[0.08, 0.08, .90, .90]]
        axis = (fig.add_axes(frames[0], frame_on=True))
        axis = [axis]
        p = 0

    scales = [np.max(np.abs(loadings.values)),
              [np.max(np.abs(scores.loc[idx].values)) for idx in ['pc1', 'pc2']]]

    # Setup biplot axis
    axis[p].set_xlabel(r'P.C. 1 ({0:.1f}% explained variation)'.format(
        np.round(eig_val[0]**2 / np.sum(eig_val**2)*100)))
    axis[p].set_ylabel(r'P.C. 2 ({0:.1f}% explained variation)'.format(
        np.round(eig_val[1]**2 / np.sum(eig_val**2)*100)))
    axis[p].set_xlim(-scales[0]*1.1, scales[0]*1.1)
    axis[p].set_ylim(-scales[0]*1.1, scales[0]*1.1)
    axis[p].plot([axis[p].get_xlim()[0], axis[p].get_xlim()[1]],
                 [0.0, 0.0], '--', color='black', alpha=0.4)
    axis[p].plot([0.0, 0.0], [axis[p].get_ylim()[0], axis[p].get_ylim()[1]],
                 '--', color='black', alpha=0.4)

    if plotloadings is not None:
        _plotloadings(axis[p], loadings, plotloadings, scales[0])

    scores = (scales[0]*(scores.T/scales[1])).T
    _plotscores(axis[p], scores, descr, legend, labels, plotellipses, colors)


def ilr_coords(data, descr=None, center=False):
    ''' Plot ILR coordinates '''
    for column in data.T:
        if np.abs(data.T[column].sum()-100.) > 1e-6:
            raise AttributeError("Error: Composition is not closed to 100.")

    if center:
        ilr = ((data/data.coda.gmean()).coda.closure(100)).coda.ilr().loc[:, [0, 1]]
    else:
        ilr = data.coda.ilr().loc[:, [0, 1]]

    bound = np.max([ilr.max().max(), -ilr.min().min()])*1.5

    fig = plt.figure(1)
    plt.clf()
    axis = plt.subplot()
    axis.set_xlabel('ILR $x_1^*$')
    axis.set_ylabel('ILR $x_2^*$')
    axis.set_xlim(-bound, bound)
    axis.set_ylim(-bound, bound)
    axis.set_aspect('equal')
    axis.plot([0, 0], [-bound, bound], '--', color='black', lw=0.5)
    axis.plot([-bound, bound], [0, 0], '--', color='black', lw=0.5)

    if descr is not None:
        for group in set(descr):
            idx = descr.loc[descr == group].index
            axis.plot(*ilr.loc[idx].T.values, 'o', alpha=0.7,
                      label=group)  # , color=descr.loc[idx[0], 'color'])
        axis.legend(frameon=False)
    else:
        axis.plot(*ilr.T.values, 'o', alpha=0.7, color='steelblue')

    points = extra.get_covariance_ellipse(ilr)
    extra.plot_covariance_ellipse(axis, points)


def ternary(data, descr=None, center=False, conf=False):
    ''' Plot ternary diagram '''
    if np.shape(data)[1] > 3:
        raise AttributeError("Error: Too many parts in composition (max. 3).")
    for column in data.T:
        print(np.abs(data.T[column].sum()-100.))
        if np.abs(data.T[column].sum()-100.) > 1e-6:
            raise AttributeError("Error: Composition is not closed to 100.")

    figure, tax = td.figure(scale=100)
    tax.boundary(linewidth=1.5)
    tax.gridlines(color="blue", multiple=10, linewidth=0.5, alpha=0.5)
    tax.left_axis_label("% {0:s}".format(data.columns[0]), fontsize=16, offset=0.14)
    tax.right_axis_label("% {0:s}".format(data.columns[1]), fontsize=16, offset=0.14)
    tax.bottom_axis_label("% {0:s}".format(data.columns[2]), fontsize=16, offset=0.12)
    tax.ticks(axis='lbr', linewidth=1, multiple=10, offset=0.03)
    tax.clear_matplotlib_ticks()
    tax.get_axes().axis('off')

    if center:
        sdata = (data/data.coda.gmean()).coda.closure(100)
    else:
        sdata = data

    if descr is not None:
        for group in set(descr):
            idx = descr.loc[descr == group].index
            tax.scatter(sdata.loc[idx, [sdata.columns[2], sdata.columns[1],
                                        sdata.columns[0]]].values, alpha=0.7)
    else:
        tax.scatter(sdata.loc[:, [sdata.columns[2], sdata.columns[1],
                                  sdata.columns[0]]].values, alpha=0.7,
                    color='steelblue')

    psi = np.array([[1, 1, -1], [1, -1, 0]])
    psip = np.array([psi[i]/np.linalg.norm(psi[i]) for i in [0, 1]])

    if conf:
        ilr = sdata.coda.ilr().loc[:, [0, 1]]
        points = extra.get_covariance_ellipse(ilr)
        psi = extra.sbp_basis(sdata)
        ellipse = pd.DataFrame(np.exp(np.matmul(points, psi))).coda.closure(100)
        ellipse = ellipse.loc[:, [ellipse.columns[1], ellipse.columns[0],
                                  ellipse.columns[2]]]
        tax.plot(ellipse.values, color='black', lw=0.5, ls='-')

    return tax
